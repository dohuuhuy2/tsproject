@echo off
echo Deleting metro cache
rd %localappdata%\Temp\metro-cache /s /q 
del %localappdata%\Temp\haste-map*
echo Cleaning build
cd android & gradlew clean & cd .. 
echo Deleting node_modules
rd node_modules /q /s 
echo Cleaning npm cache
npm cache clean --force 
npm install
echo Done


  "clear": "rm -rf $TMPDIR/react-native-packager-cache-* && rm -rf $TMPDIR/metro-bundler-cache-* && npm cache clean --force && npm start -- --reset-cache"