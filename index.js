/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import MyApp from './src/App';

AppRegistry.registerComponent(appName, () => MyApp);
