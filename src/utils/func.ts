export const ScreenToUri = ({name}: {name: string}) => {
  return `/${name}`;
};
