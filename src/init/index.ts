export const SCREEN = {
  HOME: 'home',
  DEMO: 'demo',
  PROFILE: 'profile',
  GOLD_PRICE: 'gold',
  PETROL_EUM: 'petroleumScreen',
  EXCHANGE_RATE: 'exchangeRate',
  WEATHER: 'weather',
  DETAIL_NEWS: 'detailNews',
  SCAN_QR: 'scanQR',
  SCAN_TEXT: 'scanText',
  CALENDAR: 'Calendar',
};
