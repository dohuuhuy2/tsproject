import { SCREEN } from '@init';
import { NavigationContainer } from '@react-navigation/native';
import {
  createNativeStackNavigator,
  NativeStackHeaderProps,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import CalendarCreen from '@screen/Calendar';
import DemoCreen from '@screen/Demo';
import ExchangeRateScreen from '@screen/ExchangeRate';
import GoldPriceScreen from '@screen/GoldPrice';
import HomeScreen from '@screen/Home';
import NewsDetail from '@screen/NewsDetail';
import PetrolEumScreen from '@screen/PetrolEum';
import ProfileScreen from '@screen/Profile';
import ScanQRScreen from '@screen/ScanQR';
import ScanTextScreen from '@screen/ScanText';
import WeatherCreen from '@screen/Weather';
import Header from '@templates/Header';

import * as React from 'react';

const Stack = createNativeStackNavigator();

function MyNavigation() {
  const header = (props: NativeStackHeaderProps) => {
    return <Header {...props} />;
  };

  const optionScreen: NativeStackNavigationOptions = {
    headerShown: true,
    header,
  };

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={SCREEN.HOME}
        screenOptions={() => ({
          headerShown: false,
          animation: 'slide_from_right',
        })}>
        <Stack.Screen
          name={SCREEN.HOME}
          component={HomeScreen}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.PROFILE}
          component={ProfileScreen}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.GOLD_PRICE}
          component={GoldPriceScreen}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.DEMO}
          component={DemoCreen}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.PETROL_EUM}
          component={PetrolEumScreen}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.EXCHANGE_RATE}
          component={ExchangeRateScreen}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.WEATHER}
          component={WeatherCreen}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.DETAIL_NEWS}
          component={NewsDetail}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.SCAN_QR}
          component={ScanQRScreen}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.SCAN_TEXT}
          component={ScanTextScreen}
          options={optionScreen}
        />
        <Stack.Screen
          name={SCREEN.CALENDAR}
          component={CalendarCreen}
          options={optionScreen}
        />
      </Stack.Navigator>
      {/* <Footer /> */}
    </NavigationContainer>
  );
}

export default MyNavigation;
