import { Dimensions } from 'react-native';

export const sizeRem = 16;

export const rem = {
  quarter: sizeRem / 4,
  half: sizeRem / 2,
  1: sizeRem,
  2: sizeRem * 2,
  3: sizeRem * 3,
  4: sizeRem * 4,
  5: sizeRem * 5,
  6: sizeRem * 6,
};

export const vw = (percentageWidth: any) => {
  return Dimensions.get('window').width * (percentageWidth / 100);
};

export const vh = (percentageHeight: any) => {
  return Dimensions.get('window').height * (percentageHeight / 100);
};

type Cell = {
  spacing?: number;
  column?: number;
  unit?: '%' | any;
};

export const grid = {
  wrapper: {
    width: '100%',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  } as any,

  cell: ({ spacing = 1, column = 2, unit = '%' }: Cell) => {
    const width = 100 / column - spacing * 2;

    return {
      margin: spacing - 0.001 + unit,
      width: width + unit,
    };
  },
};
