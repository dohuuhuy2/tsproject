import axios, {AxiosInstance, AxiosRequestConfig} from 'axios';

const configs: AxiosRequestConfig = {
  timeout: 5000,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    'Access-Control-Allow-Credentials': true,
    'Content-Type': 'application/json',
  },
};

export const client: AxiosInstance = axios.create(configs);
