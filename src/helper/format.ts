export const format = {
  money: (text = 0) => {
    return text.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });
  },
  money2: (str: any, currency = '') => {
    const parts = str.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    return parts.join(',') + ' ' + currency;
  },
};
