import Clipboard from '@react-native-community/clipboard';

export const copyToClipboard = async ({ value = '' }) => {
  try {
    console.log('value', value);
    Clipboard.setString(value);
  } catch (error) {
    console.log('error :>> ', error);
  }
};
