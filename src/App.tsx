import { ThemeProvider } from '@rneui/themed';
import { persistor, store } from '@store/rootStore';
import * as React from 'react';
import { SafeAreaView } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider as StoreProvider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import MyNavigation from './MyNavigation';
import theme from './theme';
import 'moment/locale/vi';

function MyApp() {
  return (
    <SafeAreaProvider style={{ flex: 1, backgroundColor: 'white' }}>
      <StoreProvider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ThemeProvider theme={theme}>
            <SafeAreaView style={{ flex: 1 }}>
              <MyNavigation />
            </SafeAreaView>
          </ThemeProvider>
        </PersistGate>
      </StoreProvider>
    </SafeAreaProvider>
  );
}

export default MyApp;
