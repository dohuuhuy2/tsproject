import InfoDateUpdate from '@atoms/InfoDateUpdate';
import TSList from '@libraries/TSList';
import actionStore from '@store/actionStore';
import { AppState } from '@store/interface';
import React from 'react';
import { ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Items from './common/Items';

const ExchangeRateScreen: React.FC<any> = ({ _navigation }) => {
  const marketPrice = useSelector((state: AppState) => state.marketPrice);

  const dispatch = useDispatch<any>();

  React.useEffect(() => {
    dispatch(actionStore.MarketPrice.ExchangeRate.Request({}));
  }, [dispatch]);

  if (0) {
    console.log('marketPrice :>> ', marketPrice);
  }

  return (
    <ScrollView style={{}}>
      <InfoDateUpdate />

      <TSList
        list={marketPrice?.exchangeRate?.data}
        renderItem={v => <Items {...v} />}
      />
    </ScrollView>
  );
};
export default ExchangeRateScreen;
