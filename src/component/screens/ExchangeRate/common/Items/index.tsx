import { View, Text } from 'react-native';
import React from 'react';
import { rem } from '@asset/styles/variables';
import { format } from '@helper/format';

const Items = ({ item }: any) => {
  return (
    <View
      style={{
        marginHorizontal: 7,
        backgroundColor: 'black',
        borderRadius: rem[1],
        padding: rem[1],
      }}>
      <Text style={{ color: 'orange', fontSize: 16, marginBottom: rem[1] }}>
        {`${item?.CurrencyName} (${item?.CurrencyCode})`}
      </Text>
      <View style={{ paddingLeft: rem[1] }}>
        <Text style={{ color: 'white' }}>
          Mua vào: {format.money2(item.Buy)}
        </Text>
        <Text style={{ color: 'white' }}>
          Bán ra: {format.money2(item.Sell)}
        </Text>
      </View>
    </View>
  );
};

export default Items;
