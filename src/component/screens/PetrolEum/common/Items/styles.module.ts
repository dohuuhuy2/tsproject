import { rem } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  items: {
    marginHorizontal: 7,
    backgroundColor: 'black',
    borderRadius: rem[1],
    padding: rem[1],
  },
});
