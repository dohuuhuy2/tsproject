import { rem } from '@asset/styles/variables';
import { format } from '@helper/format';
import React from 'react';
import { Text, View } from 'react-native';
import { styles } from './styles.module';

const Items = ({ item }: any) => {
  return (
    <View style={[styles.items]}>
      <Text
        style={{
          color: 'orange',
          fontSize: 16,
          marginBottom: rem[1],
        }}>
        LOẠI XĂNG DẦU: {item?.Name}
      </Text>
      <View style={{ paddingLeft: rem[1] }}>
        <Text style={{ color: 'white' }}>
          Giá vùng 1: {format.money2(item.Price)} {item.Units}
        </Text>
        <Text style={{ color: 'white' }}>
          Giá vùng 2: {format.money2(item.PriceRegionTwo)} {item.Units}
        </Text>
      </View>
    </View>
  );
};

export default Items;
