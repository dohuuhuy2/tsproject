import InfoDateUpdate from '@atoms/InfoDateUpdate';
import TSList from '@libraries/TSList';
import actionStore from '@store/actionStore';
import { AppState } from '@store/interface';
import { filter } from 'lodash';
import React from 'react';
import { ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Items from './common/Items';

const PetrolEumScreen = () => {
  const marketPrice = useSelector((state: AppState) => state.marketPrice);

  const dispatch = useDispatch<any>();

  React.useEffect(() => {
    dispatch(actionStore.MarketPrice.PetrolEum.Request({}));
  }, [dispatch]);

  return (
    <ScrollView style={{}}>
      <InfoDateUpdate />

      <TSList
        list={filter(marketPrice.petrolEum.data, { Type: 'PETRO' })}
        renderItem={v => <Items {...v} />}
        column={1}
        spacing={1}
      />
    </ScrollView>
  );
};
export default PetrolEumScreen;
