import { rem } from '@asset/styles/variables';
import { Text } from '@rneui/base';
import React from 'react';
import {
  ScrollView,
  StyleSheet,
  useWindowDimensions,
  View,
} from 'react-native';
import HTML from 'react-native-render-html';

const NewsDetail = (props: any) => {
  const contentWidth = useWindowDimensions().width;
  const {
    route: {
      params: { item },
    },
  } = props;

  return (
    <ScrollView style={{ backgroundColor: 'white' }}>
      <View
        style={{
          padding: rem[1],
        }}>
        <Text h4 style={{ marginVertical: rem[1], marginBottom: '15%' }}>
          {item.title}
        </Text>
        <Text
          style={{
            backgroundColor: 'snow',
            padding: rem[1],
            borderLeftColor: 'blue',
            borderLeftWidth: 5,
            borderRadius: 8,
          }}>
          {item.description}
        </Text>
        <HTML source={{ html: item.content }} contentWidth={contentWidth} />
      </View>
    </ScrollView>
  );
};

export default NewsDetail;

export const styles = StyleSheet.create({});
