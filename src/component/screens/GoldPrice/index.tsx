import InfoDateUpdate from '@atoms/InfoDateUpdate';
import TSList from '@libraries/TSList';
import actionStore from '@store/actionStore';
import { AppState } from '@store/interface';
import React from 'react';
import { ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Items from './common/Items';

const GoldPriceScreen: React.FC<any> = ({ _navigation }) => {
  const marketPrice = useSelector((state: AppState) => state.marketPrice);

  const dispatch = useDispatch<any>();

  React.useEffect(() => {
    dispatch(actionStore.MarketPrice.GoldPrice.Request({}));
  }, [dispatch]);

  return (
    <ScrollView style={{}}>
      <InfoDateUpdate />

      <TSList
        list={marketPrice.goldPrice.data}
        renderItem={v => <Items {...v} />}
      />
    </ScrollView>
  );
};
export default GoldPriceScreen;
