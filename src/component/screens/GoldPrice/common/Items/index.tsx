import { View, Text } from 'react-native';
import React from 'react';
import { rem } from '@asset/styles/variables';
import { format } from '@helper/format';

const Items = ({ item, index: i }: any) => {
  return (
    <View
      style={{
        marginHorizontal: 7,
        backgroundColor: 'black',
        borderRadius: rem[1],
        padding: rem[1],
      }}>
      <Text style={{ color: 'orange', fontSize: 16, marginBottom: rem[1] }}>
        {item[`@n_${i + 1}`]}
      </Text>
      <View style={{ paddingLeft: rem[1] }}>
        <Text style={{ color: 'white' }}>
          Giá mua vào: {format.money2(item[`@pb_${i + 1}`])}
        </Text>
        <Text style={{ color: 'white' }}>
          Giá bán ra: {format.money2(item[`@ps_${i + 1}`])} VND
        </Text>
        <Text style={{ color: 'white' }}>
          Giá thế giới: {format.money2(item[`@pt_${i + 1}`])} VND
        </Text>
        <Text style={{ color: 'white' }}>
          Thời gian nhập giá vàng : {item[`@d_${i + 1}`]}
        </Text>
      </View>
    </View>
  );
};

export default Items;
