import { rem } from '@asset/styles/variables';
import { AppState } from '@store/interface';
import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { useSelector } from 'react-redux';

const ProfileScreen: React.FC<any> = ({ _navigation }) => {
  const total = useSelector((state: AppState) => state.total);

  if (0) {
    console.log('total :>> ', total);
  }

  return (
    <ScrollView style={{}}>
      <View
        style={{
          backgroundColor: 'gray',
          minHeight: 200,
          padding: rem[1],
        }}>
        <Text>cá nhân</Text>
      </View>
    </ScrollView>
  );
};
export default ProfileScreen;
