import { rem } from '@asset/styles/variables';
import { copyToClipboard } from '@helper/copy';
import React, { Fragment } from 'react';
import { Linking, Text, TouchableOpacity, View } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from './styles.module';

export type Props = {
  name?: string;
  basenum?: number;
};

const ScanQRScreen: React.FC<Props> = () => {
  const [state, setState] = React.useState<any>({
    scan: false,
    ScanResult: false,
    result: null,
  });

  const onSuccess = (e: any) => {
    const check = e.data.substring(0, 4);
    setState({
      result: e,
      scan: false,
      ScanResult: true,
    });
    if (check === 'http') {
      Linking.openURL(e.data).catch(err =>
        console.error('An error occured', err),
      );
    } else {
      setState({
        result: e,
        scan: false,
        ScanResult: true,
      });
    }
  };

  const activeQR = () => {
    setState({ scan: !state.scan });
  };

  const scanAgain = () => {
    setState({ scan: true, ScanResult: false });
  };

  const { scan, ScanResult, result } = state;
  return (
    <View style={styles.scrollViewStyle}>
      <React.Fragment>
        {scan && (
          <View style={styles.header}>
            <TouchableOpacity onPress={() => activeQR()}>
              <Text style={{ color: 'white' }}>Quay lại</Text>
            </TouchableOpacity>
          </View>
        )}

        {!scan && !ScanResult && (
          <View style={styles.cardView}>
            <Text style={styles.descText}>
              Vui lòng di chuyển máy ảnh của bạn {'\n'} qua Mã QR
            </Text>

            <TouchableOpacity onPress={activeQR} style={styles.buttonScan}>
              <View style={styles.buttonWrapper}>
                <Text style={{ ...styles.buttonTextStyle, color: '#2196f3' }}>
                  Scan QR Code
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        )}

        {ScanResult && (
          <Fragment>
            <Text style={styles.textTitle1}>Kết quả</Text>
            <View style={ScanResult ? styles.scanCardView : styles.cardView}>
              <Text selectable style={{ marginVertical: rem.half }}>
                Type : {result?.type}
              </Text>
              <Text
                selectable
                style={{ marginVertical: rem.half }}
                onPress={() => copyToClipboard({ value: result?.data })}>
                Kết quả : {result?.data}
              </Text>
              <Text selectable style={{ marginVertical: rem.half }}>
                RawData: {result?.rawData}
              </Text>
              <TouchableOpacity onPress={scanAgain} style={styles.buttonScan}>
                <View style={styles.buttonWrapper}>
                  <Text style={{ ...styles.buttonTextStyle, color: '#2196f3' }}>
                    Bấm để quét lại
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </Fragment>
        )}

        {scan && (
          <QRCodeScanner
            containerStyle={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            cameraStyle={{
              width: '100%',
              padding: rem[1],
              borderRadius: rem[1],
            }}
            reactivate={true}
            showMarker={true}
            onRead={onSuccess}
            topContent={
              <Text style={styles.centerText}>
                Vui lòng di chuyển máy ảnh của bạn {'\n'} qua Mã QR
              </Text>
            }
          />
        )}
      </React.Fragment>
    </View>
  );
};

export default ScanQRScreen;
