import { rem } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    padding: rem[1],
    backgroundColor: 'white',
    margin: rem[1],
    borderRadius: rem.quarter,
  },
});
