import React from 'react';
import { ScrollView, View } from 'react-native';
import Days from './common/Days';
import HeaderCalendar from './common/HeaderCalendar';
import useDataCalendar from './common/HOCs/useDataCalendar';
import WeekDays from './common/WeekDays';
import { styles } from './styles.module';

const CalendarCreen = () => {
  const { date, handleNext, handlePrev, dayss } = useDataCalendar();

  const methodHeader = { date, handleNext, handlePrev };
  const methodDays = { dayss };

  return (
    <ScrollView>
      <View style={styles.container}>
        <HeaderCalendar {...methodHeader} />
        <WeekDays />
        <Days {...methodDays} />
      </View>
    </ScrollView>
  );
};

export default CalendarCreen;
