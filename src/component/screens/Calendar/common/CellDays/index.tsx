import React from 'react';
import { Text, View } from 'react-native';
import { defineDays } from '../contanst';
import { styles } from './styles.module';

const CellDays = ({ value, type, isTodays }: any) => {
  const styleToday = isTodays ? styles.today : {};

  const handleStyle = () => {
    switch (type) {
      case defineDays.CELL_WEEK:
        return {
          cell: styles.cellWeek,
          txt: styles.cellTxtWeek,
        };

      case defineDays.NO_DAYS_IN_MONTH:
        return {
          cell: {},
          txt: styles.txtFaded,
        };

      case defineDays.DAYS_IN_MONTH:
        return {
          cell: {},
          txt: styles.txtCell,
        };

      default:
        return { cell: {}, txt: {} };
    }
  };

  return (
    <View style={[handleStyle().cell, styleToday, styles.cell]}>
      <Text style={[handleStyle().txt]}>{value}</Text>
    </View>
  );
};

export default CellDays;
