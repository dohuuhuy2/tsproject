import { rem } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  cellWeek: {
    textAlign: 'center',
    padding: rem.half,
  },
  cell: {
    textAlign: 'center',
    paddingVertical: rem[1],
    borderRadius: rem.half,
    // backgroundColor: '#fff1df',
    alignItems: 'center',
  },
  today: {
    backgroundColor: '#fff1df',
  },
  faded: {},
  txtCell: {
    textAlign: 'center',
    fontSize: 12,
    // color: 'red',
  },
  cellTxtWeek: {
    textAlign: 'center',
    fontSize: 12,
  },
  txtFaded: {
    color: '#dee2e6',
  },
});
