import { rem } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    alignItems: 'center',
  },
  txtDate: {
    fontSize: 24,
    fontWeight: '500',
  },
  groupBtn: {
    display: 'flex',
    flexDirection: 'row',
  },
  btn: {
    margin: rem.half,
    borderRadius: rem.half,
    color: 'black',
  },
  txtBtn: {
    color: 'gray',
  },
});
