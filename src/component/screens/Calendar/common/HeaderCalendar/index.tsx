import { Button } from '@rneui/base';
import React from 'react';
import { Text, View } from 'react-native';
import { styles } from './styles.module';

export type Props = {
  date: moment.Moment;
  handleNext: () => void;
  handlePrev: () => void;
};

const HeaderCalendar = ({ date, handlePrev, handleNext }: Props) => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.txtDate}> Tháng {date.format('MM - YYYY')}</Text>
      </View>
      <View style={styles.groupBtn}>
        <Button
          color={'#f3f4fc'}
          buttonStyle={styles.btn}
          titleStyle={styles.txtBtn}
          onPress={handlePrev}>
          prev
        </Button>
        <Button
          color={'#f5faf5'}
          buttonStyle={styles.btn}
          titleStyle={styles.txtBtn}
          onPress={handleNext}>
          next
        </Button>
      </View>
    </View>
  );
};

export default HeaderCalendar;
