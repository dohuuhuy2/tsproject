import TSList from '@libraries/TSList';
import React from 'react';
import { View } from 'react-native';
import CellDays from '../CellDays';
import { styles } from './styles.module';

const Days = ({ dayss }: { dayss: never[] }) => {
  return (
    <View style={styles.containerDays}>
      <TSList
        list={dayss}
        renderItem={({ item }) => {
          return (
            <CellDays
              value={item.days}
              isTodays={item.isToday}
              type={item.type}
            />
          );
        }}
        column={7}
      />
    </View>
  );
};

export default Days;
