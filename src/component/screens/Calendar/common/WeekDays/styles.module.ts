import { rem } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  weekCell: {
    backgroundColor: '#fef6f4',
    marginBottom: rem[1],
  },
});
