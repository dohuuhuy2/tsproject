import TSList from '@libraries/TSList';
import React from 'react';
import { View } from 'react-native';
import CellDays from '../CellDays';
import { defineDays, weekDays } from '../contanst';
import { styles } from './styles.module';

const WeekDays = () => {
  return (
    <View style={styles.weekCell}>
      <TSList
        list={weekDays}
        renderItem={({ item }) => {
          return <CellDays value={item} type={defineDays.CELL_WEEK} />;
        }}
        column={7}
      />
    </View>
  );
};

export default WeekDays;
