import moment from 'moment';
import React from 'react';
import { calcDays, dataDate } from '../func';

const useDataCalendar = () => {
  const [date, setDate] = React.useState(moment());

  const handlePrev = () => {
    setDate(v => moment(v).subtract(1, 'month'));
  };

  const handleNext = () => {
    setDate(v => moment(v).add(1, 'month'));
  };

  return {
    date,
    dayss: calcDays(dataDate({ date })),
    handlePrev,
    handleNext,
  };
};

export default useDataCalendar;
