import { Button } from '@rneui/themed';
import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { styles } from './styles.module';

export type Props = {
  name?: string;
  basenum?: number;
};

const DemoCreen: React.FC<Props> = ({
  name = 'Tăng Giảm Số Lượng',
  basenum = 0,
}) => {
  const [num, setnum] = React.useState(basenum);

  const onIncrement = () => setnum(num + 1);
  const onDecrement = () => setnum(num > 0 ? num - 1 : 0);

  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.greeting}>Demo {name}</Text>

        <View style={styles.card}>
          <Text style={styles.greeting}>Số lượng: {num}</Text>
          <View style={styles.groupBtn}>
            <Button
              title="Tăng"
              accessibilityLabel="increment"
              onPress={onIncrement}
              color="blue"
              buttonStyle={{
                borderRadius: 8,
                backgroundColor: 'rgba(127, 220, 103, 1)',
              }}
              containerStyle={{
                width: 200,
                height: 40,
                marginHorizontal: 50,
                marginVertical: 10,
              }}
            />
            <Button
              buttonStyle={{
                borderRadius: 8,
                backgroundColor: 'rgba(255, 193, 7, 1)',
              }}
              containerStyle={{
                width: 200,
                height: 40,
                marginHorizontal: 50,
                marginVertical: 10,
              }}
              title="Giảm"
              accessibilityLabel="decrement"
              onPress={onDecrement}
              color="red"
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default DemoCreen;
