import { rem } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  image: { flex: 1, justifyContent: 'center' },
  container: {
    flex: 1,
    marginTop: 8,
    backgroundColor: 'aliceblue',
    margin: 5,
    borderRadius: 8,
  },
  box: {
    width: 50,
    height: 50,
  },
  boxService: {
    display: 'flex',
    backgroundColor: 'aliceblue',
    padding: rem[1],
    marginHorizontal: rem[1],
    margin: rem[1],
    borderRadius: rem[1],
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },

  row: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  button: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 8,
    backgroundColor: 'skyblue',
    alignSelf: 'flex-start',
    marginHorizontal: '1%',
    marginBottom: 8,
    minWidth: '48%',
    textAlign: 'center',
  },
  selected: {
    backgroundColor: 'coral',
    borderWidth: 0,
  },
  buttonLabel: {
    fontSize: 16,
    fontWeight: '600',
    color: 'white',
    textAlign: 'center',
  },
  selectedLabel: {
    color: 'white',
  },
  label: {
    textAlign: 'center',
    marginBottom: rem[1],
    fontSize: 24,
    fontWeight: '500',
    color: 'skyblue',
  },
});
