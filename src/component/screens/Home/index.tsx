import BoxService from '@organisms/BoxService';
import News from '@organisms/News';
import Banner from '@templates/Banner';
import React from 'react';
import { ScrollView } from 'react-native';

const HomeScreen = () => {
  return (
    <>
      <ScrollView style={{ backgroundColor: 'white' }}>
        <Banner />
        <BoxService />
        <News />
      </ScrollView>
    </>
  );
};

export default HomeScreen;
