import { copyToClipboard } from '@helper/copy';
import { requestCameraPermission } from '@helper/permission/camera';
import { Image } from '@rneui/base';
import React, { useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import RNTextDetector from 'rn-text-detector';
import ActionScanText from './common/ActionScanText';
import { styles } from './styles.module';

const ScanTextScreen: React.FC = () => {
  const [state, setState] = useState<any>({
    loading: false,
    image: null,
    textRecognition: null,
    toast: {
      message: '',
      isVisible: false,
    },
  });

  const onPress = ({ type }: { type: 'capture' | 'library' }) => {
    setState({ ...state, loading: true });
    if (type === 'capture') {
      requestCameraPermission({
        callBack: () => launchCamera({ mediaType: 'photo' }, onImageSelect),
      });
    } else {
      launchImageLibrary({ mediaType: 'photo' }, onImageSelect);
    }
  };

  const onImageSelect = async (media: any) => {
    console.log('media :>> ', media);
    if (!!media && media.assets) {
      const file = media.assets[0].uri;
      const textRecognition = await RNTextDetector.detectFromUri(file);
      console.log('textRecognition :>> ', textRecognition);
      setState({
        ...state,
        textRecognition,
        image: file,
        loading: false,
      });
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{}}>
        <ActionScanText onPress={onPress} />

        {state.textRecognition && (
          <ScrollView style={styles.result}>
            {state.image && (
              <View style={styles.thumbnail}>
                <Image
                  style={styles.image}
                  source={{ uri: state.image }}
                  resizeMode="contain"
                  resizeMethod="auto"
                />
              </View>
            )}

            <View>
              {state.textRecognition?.map(
                (item: { text: string }, i: number) => {
                  return (
                    <TouchableOpacity
                      onPress={() => copyToClipboard({ value: item.text })}
                      key={i}>
                      <Text style={{ lineHeight: 24 }} selectable>
                        {item.text}
                      </Text>
                    </TouchableOpacity>
                  );
                },
              )}
            </View>
          </ScrollView>
        )}
      </View>
    </SafeAreaView>
  );
};

export default ScanTextScreen;
