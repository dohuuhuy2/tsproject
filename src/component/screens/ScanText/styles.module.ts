import { grid, rem, vh } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    backgroundColor: '#2196f3',
    padding: rem[1],
    margin: rem[1],
    borderRadius: rem.half,
    color: 'white',
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
    marginBottom: rem[1],
    color: 'white',
    fontWeight: '600',
  },
  groupBtn: {
    ...grid.wrapper,
    // backgroundColor: 'red',
  },
  button: {
    ...grid.cell({ margin: 1, column: 2 }),
    backgroundColor: 'blue',
    padding: rem[1],
    borderRadius: rem.half,
    textAlign: 'center',
    alignItems: 'center',
  },
  txtBtn: {
    color: 'white',
  },
  shadow: {},
  thumbnail: {
    margin: rem[1],
    padding: rem[1],
    alignItems: 'center',
    // backgroundColor: 'red',
  },

  image: { minWidth: 400, minHeight: 300 },
  result: {
    backgroundColor: '#e8e8e8',
    padding: rem[1],
    marginVertical: rem[1],
    marginTop: rem[2],
    borderRadius: rem.quarter,
    maxHeight: vh(65),
  },
});
