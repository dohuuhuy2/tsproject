import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { styles } from './styles.module';

export interface Props {
  onPress: ({ type }: { type: 'capture' | 'library' }) => void;
}

const ActionScanText = ({ onPress }: Props) => {
  return (
    <View style={styles.groupBtn}>
      <TouchableOpacity
        style={[styles.button, styles.shadow]}
        onPress={() => onPress({ type: 'capture' })}>
        <Text style={styles.txtBtn}>Máy ảnh</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={[styles.button, styles.shadow]}
        onPress={() => onPress({ type: 'library' })}>
        <Text style={styles.txtBtn}>Hình</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ActionScanText;
