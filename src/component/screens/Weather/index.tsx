import ChoiceCityWeather from '@organisms/ChoiceCityWeather';
import WeatherByCity from '@organisms/WeatherByCity';
import actionStore from '@store/actionStore';
import React from 'react';
import { ScrollView } from 'react-native';
import { useDispatch } from 'react-redux';

export type Props = {
  name?: string;
  basenum?: number;
};

const WeatherCreen: React.FC<Props> = ({}) => {
  const dispatch = useDispatch<any>();

  React.useEffect(() => {
    dispatch(actionStore.MarketPrice.Weather.Request({}));
  }, [dispatch]);
  return (
    <>
      <ChoiceCityWeather />
      <ScrollView>
        <WeatherByCity />
      </ScrollView>
    </>
  );
};

export default WeatherCreen;
