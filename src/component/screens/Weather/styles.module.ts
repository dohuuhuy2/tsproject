import { rem } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {},
  card: {
    backgroundColor: 'rgba(90, 154, 230, 1)',
    padding: rem[1],
    borderRadius: rem[1],
    marginHorizontal: rem[1],
    marginVertical: rem[1],
  },
  groupBtn: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginVertical: 20,
  },
  greeting: {
    fontSize: 14,
    fontWeight: '500',
    margin: 16,
    textAlign: 'center',
  },
});
