import { rem } from '@asset/styles/variables';
import React from 'react';
import { ImageBackground, StyleSheet, View } from 'react-native';

const Banner = () => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#ceecfe',
        borderRadius: rem[1],
        margin: rem[1],
        padding: rem[1],
        position: 'relative',
      }}>
      <View style={{}}>
        <ImageBackground
          source={{
            uri: 'https://co.sshop.live/client/assets/images/other/Lovepik_com-611591520-Vector business office theme illustration.png',
          }}
          style={{
            minHeight: 180,
          }}
          resizeMode={'contain'}
        />
      </View>
    </View>
  );
};

export default Banner;

export const styles = StyleSheet.create({});
