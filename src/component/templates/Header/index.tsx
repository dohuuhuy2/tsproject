import { rem } from '@asset/styles/variables';
import { SCREEN } from '@init';
import { useLinkTo } from '@react-navigation/native';
import { NativeStackHeaderProps } from '@react-navigation/native-stack';
import { ScreenToUri } from '@utils/func';
import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { handlerViewTitle } from './common/handler';
import { styles } from './styles.module';

const Header = (props: NativeStackHeaderProps) => {
  const linkTo = useLinkTo();

  return (
    <View
      style={[
        styles.container,
        {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '100%',
        },
      ]}>
      <View
        style={[
          styles.logo,
          {
            width: '20%',
          },
        ]}>
        <TouchableOpacity
          style={{ padding: rem[1] }}
          onPress={() => {
            linkTo(ScreenToUri({ name: SCREEN.HOME }));
          }}>
          <Image
            style={{
              resizeMode: 'contain',
              height: 60,
              width: 60,
            }}
            source={require('static/images/huyi.png')}
          />
        </TouchableOpacity>
      </View>
      <View style={{ padding: rem[1], width: '60%' }}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 16,
            letterSpacing: 1,
            fontWeight: '600',
          }}>
          {handlerViewTitle({ key: props.route.name })?.label}
        </Text>
      </View>
      <View style={{ padding: rem[1], width: '20%' }}>
        <Text style={{ textAlign: 'right', display: 'none' }}>sShop</Text>
      </View>
    </View>
  );
};

export default Header;
