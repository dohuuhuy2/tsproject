import { SCREEN } from '@init';
import { listService } from '@organisms/BoxService/common/data';
import { find } from 'lodash';

export const listTitleHeader = [
  ...listService,
  {
    key: SCREEN.HOME,
    label: '',
  },
  {
    key: SCREEN.DETAIL_NEWS,
    label: 'Tin Tức Số',
  },
];

export const handlerViewTitle = ({ key }: any) => {
  const findViewTitle = find(listTitleHeader, { key });
  if (findViewTitle) {
    return findViewTitle;
  } else {
    return null;
  }
};
