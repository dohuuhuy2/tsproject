import { rem } from '@asset/styles/variables';
import { Button, Icon } from '@rneui/base';
import actionStore from '@store/actionStore';
import { AppState } from '@store/interface';
import { random } from 'lodash';
import { Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import React from 'react';

const CardTestReduxSaga = () => {
  const dispatch = useDispatch<any>();

  const total = useSelector((state: AppState) => state.total);

  const handleDemo = () => {
    dispatch(actionStore.TotalData.Demo.Save({ data: random(1000) }));
  };
  return (
    <View
      style={{
        display: 'flex',
        minHeight: 200,
        backgroundColor: 'white',
        margin: rem[1],
        borderRadius: rem[1],
        padding: rem[1],
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
      }}>
      <View style={{ marginBottom: rem[1] }}>
        <Icon
          raised
          name="heartbeat"
          type="font-awesome"
          color="#f50"
          onPress={() => console.log('hello')}
        />
      </View>

      <Text
        style={{
          marginBottom: rem[1],
          textAlign: 'center',
          fontSize: 16,
          textTransform: 'uppercase',
        }}>
        Random Số Lượng: {total?.demo?.data}
      </Text>

      <Button
        onPress={handleDemo}
        containerStyle={{
          borderRadius: 8,
          marginVertical: rem[1],
          minWidth: 200,
        }}
        buttonStyle={{
          backgroundColor: '#dc3545',
        }}
        titleStyle={{
          marginHorizontal: 7,
        }}
        icon={
          <Icon
            style={{ marginHorizontal: 7 }}
            name="heartbeat"
            type="font-awesome"
            color="white"
          />
        }>
        random redux persist saga
      </Button>
    </View>
  );
};

export default CardTestReduxSaga;
