import { View, Text } from 'react-native';
import React from 'react';
import moment from 'moment';
import { rem } from '@asset/styles/variables';

const InfoDateUpdate = () => {
  return (
    <View
      style={{
        padding: rem[1],
        backgroundColor: 'snow',
      }}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          fontWeight: 'bold',
          fontFamily: 'Vacaciones',
        }}>
        Cập nhật ngày {'\n'}
        <Text style={{ fontStyle: 'italic', fontSize: 12 }}>
          {moment().format('LLL')}
        </Text>
      </Text>
    </View>
  );
};

export default InfoDateUpdate;
