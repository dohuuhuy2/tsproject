import { rem } from '@asset/styles/variables';
import TSList from '@libraries/TSList';
import { Text } from '@rneui/base';
import actionStore from '@store/actionStore';
import { AppState } from '@store/interface';
import React from 'react';
import { View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Items from './common/Items';
import { styles } from './styles.module';

const News = () => {
  const dispatch = useDispatch<any>();
  const { news } = useSelector((state: AppState) => state.marketPrice);

  React.useEffect(() => {
    dispatch(actionStore.MarketPrice.News.Request({}));
  }, [dispatch]);

  return (
    <View style={styles.container}>
      <Text
        style={{
          padding: rem[1],
          fontSize: 22,
          fontWeight: 'bold',
        }}>
        Tin tức số
      </Text>

      <TSList list={news.items} renderItem={v => <Items {...v} />} />
    </View>
  );
};

export default News;
