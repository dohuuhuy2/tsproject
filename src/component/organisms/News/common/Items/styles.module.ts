import { rem, vw } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  item: {
    // marginBottom: 10,
    borderRadius: rem.quarter,
  },
  itemCard: {
    borderRadius: rem.quarter,
    flexDirection: 'row',
    backgroundColor: 'white',
    elevation: 0,
    padding: rem[1],
  },
  cardView: {
    height: 100,
    borderRadius: rem.quarter,
    width: vw(40),
  },
  cardBody: {
    width: vw(55.5),
    paddingHorizontal: rem.half,
  },
  title: {
    fontWeight: '600',
    flexWrap: 'wrap',
    flexShrink: 1,
  },
  date: { fontSize: 11, marginTop: 8, fontStyle: 'italic' },
});
