import { View, Text, TouchableOpacity } from 'react-native';
import React from 'react';
import { styles } from './styles.module';
import { Image } from '@rneui/base';
import { Link } from '@react-navigation/native';
import { SCREEN } from '@init';

const Items = ({ item }: any) => {
  return (
    <TouchableOpacity style={styles.item}>
      <View style={styles.itemCard}>
        <Image source={{ uri: item.enclosure.link }} style={styles.cardView} />
        <View style={styles.cardBody}>
          <Link to={{ screen: SCREEN.DETAIL_NEWS, params: { item } }}>
            <Text style={styles.title} numberOfLines={5}>
              {item.title}
            </Text>
          </Link>
          <Text style={styles.date}>{item.pubDate}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Items;
