import TSList from '@libraries/TSList';
import { useLinkTo } from '@react-navigation/native';
import { Text } from '@rneui/base';
import { ScreenToUri } from '@utils/func';
import React from 'react';
import { View } from 'react-native';
import { listService } from './common/data';
import Items from './common/Items';
import { styles } from './styles.module';

const BoxService = () => {
  const linkTo = useLinkTo();

  const handleSelectItem = ({ item }: any) => {
    linkTo(ScreenToUri({ name: item.key }));
  };

  return (
    <React.Fragment>
      <View style={styles.boxService}>
        <Text style={styles.label}>Thông tin</Text>

        <TSList
          list={listService}
          renderItem={v => <Items {...v} />}
          column={4}
          selectItem={handleSelectItem}
        />
      </View>
    </React.Fragment>
  );
};

export default BoxService;
