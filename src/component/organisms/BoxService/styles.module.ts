import { rem } from '@asset/styles/variables';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  boxService: {
    display: 'flex',
    marginHorizontal: rem[1],
    margin: rem[1],
    // backgroundColor: 'red',
  },
  label: {
    marginBottom: rem[1],
    fontSize: 22,
    fontWeight: 'bold',
  },
});
