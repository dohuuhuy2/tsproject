import { SCREEN } from '@init';

export const listService = [
  {
    key: SCREEN.GOLD_PRICE,
    label: 'Giá Vàng',
    img: 'https://co.sshop.live/client/assets/images/other/b2b-strategy-commercial-transaction-partner-agreement-partnership-arrangement-successful-collaboration-businessmen-shaking-hands-cartoon-characters-vector-isolated-concept-metaphor-illustration_335657.png',
    bgcl: '#fef6f4',
  },
  {
    key: SCREEN.SCAN_QR,
    label: 'Scan QR',
    img: 'https://co.sshop.live/client/assets/images/other/4223371-removebg-preview.png',
    bgcl: '#fff1df',
  },
  {
    key: SCREEN.SCAN_TEXT,
    label: 'Scan Text',
    img: 'https://co.sshop.live/client/assets/images/other/4223371-removebg-preview.png',
    bgcl: '#f3f4fc',
  },
  {
    key: SCREEN.PETROL_EUM,
    label: 'Giá Xăng Dầu',
    img: 'https://co.sshop.live/client/assets/images/other/gas-removebg-preview.png',
    bgcl: '#f5faf5',
  },
  {
    key: SCREEN.EXCHANGE_RATE,
    label: 'Giá Ngoại Tệ',
    img: 'https://co.sshop.live/client/assets/images/other/b2b-strategy-commercial-transaction-partner-agreement-partnership-arrangement-successful-collaboration-businessmen-shaking-hands-cartoon-characters-vector-isolated-concept-metaphor-illustration_335657.png',
    bgcl: '#f3f4fc',
  },
  {
    key: SCREEN.WEATHER,
    label: 'Thời tiết',
    img: 'https://co.sshop.live/client/assets/images/other/weather.png',
    bgcl: '#f5faf5',
  },
  {
    key: SCREEN.CALENDAR,
    label: 'Lịch',
    img: 'https://co.sshop.live/client/assets/images/other/oil-refining-industry-gasoline-business-fuel-company-petrol-station-receiving-money-from-benzine-sale-petroleum-revenue-metaphors_335657-3381-removebg-preview.png',
    bgcl: '#fff1df',
  },
  {
    key: SCREEN.PROFILE,
    label: 'Cá nhân',
    img: 'https://co.sshop.live/client/assets/images/other/oil-refining-industry-gasoline-business-fuel-company-petrol-station-receiving-money-from-benzine-sale-petroleum-revenue-metaphors_335657-3381-removebg-preview.png',
    bgcl: '#fef6f4',
  },
];
