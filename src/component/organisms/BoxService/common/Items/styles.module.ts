import { StyleSheet } from 'react-native';

const size = 90;

export const styles = StyleSheet.create({
  itemService: {
    paddingVertical: 8,
    borderRadius: 8,
    textAlign: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignContent: 'space-between',
    alignItems: 'center',
  },
  labelItem: {
    fontSize: 12,
    fontWeight: '600',
    textAlign: 'center',
    padding: 7,
    margin: 7,
    color: '#6c757d',
    height: 45,
  },
  iconItem: {
    width: size,
    height: size,
  },
});
