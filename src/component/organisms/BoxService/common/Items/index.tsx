import { renderItemProps } from '@libraries/TSList/interface';
import { Image } from '@rneui/base';
import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { styles } from './styles.module';

export type ItemServiceProps = {
  item: any;
  index: any;
  onPress?: any;
} & renderItemProps;

const Items = ({ item, selectItem }: ItemServiceProps) => {
  return (
    <TouchableOpacity
      style={[
        styles.itemService,
        {
          backgroundColor: item.bgcl,
        },
      ]}
      onPress={() => selectItem({ item })}>
      {item?.label && (
        <Text style={[styles.labelItem]} numberOfLines={2}>
          {item?.label}
        </Text>
      )}

      {item?.img && (
        <Image
          source={{ uri: item?.img }}
          style={styles.iconItem}
          resizeMethod="scale"
        />
      )}
    </TouchableOpacity>
  );
};

export default Items;
