import actionStore from '@store/actionStore';
import { AppState } from '@store/interface';
import { find } from 'lodash';
import React, { useState } from 'react';
import { Text, View } from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useDispatch, useSelector } from 'react-redux';
import { styles } from './styles.module';

const ChoiceCityWeather = () => {
  const [isFocus, setIsFocus] = useState(false);

  const { weather } = useSelector((state: AppState) => state.marketPrice);
  const dispatch = useDispatch<any>();

  React.useEffect(() => {
    if (!weather.selectItem?.City) {
      const findItem = find(weather.data, { CityShort: 'tphochiminh' });
      dispatch(actionStore.MarketPrice.Weather.selected({ data: findItem }));
    }
  }, [dispatch, weather.data, weather.selectItem?.City]);

  const renderLabel = () => {
    if (!weather.selectItem?.City || isFocus) {
      return (
        <Text style={[styles.label, isFocus && { color: 'blue' }]}>
          Chọn tỉnh/thành
        </Text>
      );
    }
    return null;
  };

  const selectItem = ({ item }: any) => {
    dispatch(actionStore.MarketPrice.Weather.selected({ data: item }));
  };

  return (
    <View style={styles.container}>
      {renderLabel()}
      <Dropdown
        style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
        placeholderStyle={styles.placeholderStyle}
        selectedTextStyle={styles.selectedTextStyle}
        inputSearchStyle={styles.inputSearchStyle}
        iconStyle={styles.iconStyle}
        data={weather?.data}
        search
        maxHeight={300}
        labelField="City"
        valueField="CityShort"
        placeholder={!isFocus ? weather.selectItem?.City : '...'}
        searchPlaceholder="Search..."
        value={weather.selectItem?.City}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        onChange={item => {
          selectItem({ item });
          setIsFocus(false);
        }}
        renderLeftIcon={() => (
          <AntDesign
            style={styles.icon}
            color={isFocus ? 'blue' : 'black'}
            name="Safety"
            size={20}
          />
        )}
      />
    </View>
  );
};

export default ChoiceCityWeather;
