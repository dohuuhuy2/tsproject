export const handleViewImageWeather = ({ CI }: any) => {
  switch (CI) {
    case 'Mây rải rác':
      return 'https://co.sshop.live/client/assets/images/weather/clouds.png';

    case 'Mưa giông':
    case 'Mưa giông vào buổi chiều':
    case 'Mây rải rác, có thể có mưa giông':
      return 'https://co.sshop.live/client/assets/images/weather/rainy.png';

    case 'Mưa giông rải rác':
      return 'https://co.sshop.live/client/assets/images/weather/rainy.png';
    default:
      return 'https://co.sshop.live/client/assets/images/weather/cloudy.png';
  }
};
