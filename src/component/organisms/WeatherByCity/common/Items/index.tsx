import { View, Text } from 'react-native';
import React from 'react';
import moment from 'moment';
import { rem } from '@asset/styles/variables';
import { Image } from '@rneui/base';
import { handleViewImageWeather } from '../handler';

const Items = ({ item, selectItem }: any) => {
  var todayDate = moment();
  var monthDate = todayDate.add(item, 'days').format('DD/MM');
  return (
    <View
      style={{
        padding: rem[1],

        alignItems: 'center',
      }}>
      <Text>{monthDate}</Text>
      <Text style={{ fontWeight: 'bold', margin: 7 }}>
        {selectItem?.[`I${item}`]}
      </Text>
      <Text>
        Nhiệt độ {selectItem?.[`MinT${item}`]}° - {selectItem?.[`MaxT${item}`]}°
      </Text>

      <Image
        source={{
          uri: handleViewImageWeather({ CI: selectItem?.[`I${item}`] }),
        }}
        resizeMode="contain"
        style={{ minHeight: 60, width: 60, marginTop: rem[1] }}
      />
    </View>
  );
};

export default Items;
