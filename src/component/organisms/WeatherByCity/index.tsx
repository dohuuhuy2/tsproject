import { rem } from '@asset/styles/variables';
import TSList from '@libraries/TSList';
import { Image } from '@rneui/base';
import { AppState } from '@store/interface';
import { range } from 'lodash';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useSelector } from 'react-redux';
import { handleViewImageWeather } from './common/handler';
import Items from './common/Items';

const WeatherByCity = () => {
  const {
    weather: { selectItem },
  } = useSelector((state: AppState) => state.marketPrice);

  return (
    <>
      <View
        style={{
          backgroundColor: 'snow',
          margin: rem[1],
          padding: rem[1],
          borderRadius: 8,
        }}>
        <Text
          style={{
            fontSize: 22,
            textAlign: 'center',
            margin: rem[1],
          }}>
          {selectItem?.City}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginTop: rem[1],
          }}>
          <View style={{ alignItems: 'center' }}>
            <Image
              source={{
                uri: handleViewImageWeather({ CI: selectItem?.CT }),
              }}
              resizeMode="contain"
              style={{ minHeight: 120, width: 120, marginBottom: rem[1] }}
            />

            <Text>Hiện tại {selectItem?.CT}°</Text>
          </View>
          <View>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 16,
                fontWeight: '600',
                marginBottom: rem[1],
              }}>
              {selectItem?.CI}
            </Text>
            <Text>
              Nhiệt độ {selectItem?.MinT}° - {selectItem?.MaxT}°
            </Text>
            <Text>Độ ẩm {selectItem?.CH}%</Text>
          </View>
        </View>
      </View>

      <View style={{ padding: rem.half }}>
        <TSList
          list={range(1, 8)}
          column={2}
          renderItem={v => <Items {...v} selectItem={selectItem} />}
          styleItem={{ backgroundColor: 'snow', borderRadius: rem.half }}
        />
      </View>
    </>
  );
};

export default WeatherByCity;

export const styles = StyleSheet.create({});
