import { applyMiddleware, compose, createStore } from 'redux';
import persistStore from 'redux-persist/es/persistStore';
import createSagaMiddleware from 'redux-saga';
import { persistedReducer } from './rootPersist';
import rootSaga from './rootSaga';

import { composeWithDevTools } from 'remote-redux-devtools';
// import {composeWithDevTools} from 'redux-devtools-extension';

const sagaMiddleware = createSagaMiddleware();
export const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const composeEnhancer = composeWithDevTools({
  realtime: true,
  port: 8000,
});

export const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware)),
);

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);
