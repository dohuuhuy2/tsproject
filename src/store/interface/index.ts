import { combinedReducers } from '@store/rootReducer';
import { store } from '@store/rootStore';

// TotalData
export * from '@store/components/TotalData/interface/action';
export * from '@store/components/TotalData/interface/initialState';
export * from '@store/components/TotalData/interface/params';
export * from '@store/components/TotalData/interface/types';

// MarketPrice
export * from '@store/components/MarketPrice/interface/action';
export * from '@store/components/MarketPrice/interface/initialState';
export * from '@store/components/MarketPrice/interface/params';
export * from '@store/components/MarketPrice/interface/types';

export type AppState = ReturnType<typeof combinedReducers>;
export type AppDispatch = typeof store.dispatch;
