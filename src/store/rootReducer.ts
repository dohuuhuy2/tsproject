import { combineReducers } from 'redux';
import { MarketPriceReducer } from './components/MarketPrice/reducer';
import { totalReducer } from './components/TotalData/reducer';

const reducers = {
  total: totalReducer,
  marketPrice: MarketPriceReducer,
};

export const combinedReducers = combineReducers(reducers);
