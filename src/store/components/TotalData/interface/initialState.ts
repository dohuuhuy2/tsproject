import { TotalDataParams } from '@store/interface';

export interface TotalDataState {
  loading: TotalDataParams.SetLoading;
  demo: TotalDataParams.Demo.Save;
}
