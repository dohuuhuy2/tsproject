// Kiểm soát tham số trong hàm thực hiện hành động ---------------------------------------------------------------
export namespace TotalDataParams {
  export interface SetLoading {
    key?: string;
    status: boolean;
    text?: string;
    subText?: string;
  }

  export namespace Demo {
    export interface Save {
      data?: any;
    }
  }
}
