import { TotalDataParams, TotalDataTypes } from '@store/interface';

export type TotalDataActions = LoadingAction | DemoAction;

// ----------------------------------------loading------------------------------------------------------

export type LoadingAction = SetLoading;
export interface SetLoading {
  type: TotalDataTypes.Loading.SET_LOADING;
  payload: TotalDataParams.SetLoading;
}

// ------------------

export type DemoAction = Savedemo;

export interface Savedemo {
  type: TotalDataTypes.Demo.Save;
  payload: TotalDataParams.Demo.Save;
}
