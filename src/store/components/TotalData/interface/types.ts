// Kiểm soát hành động -----------------------------------------------------------------------------------------
export namespace TotalDataTypes {
  export enum Loading {
    SET_LOADING = 'SET_LOADING',
  }

  export enum Demo {
    Request = 'Demo -> Request',
    Response = 'Demo -> Response',
    Save = 'Demo -> Save',
    Selected = 'Demo -> Selected',
    Reset = 'Demo -> Reset',
  }
}
