import {
  TotalDataActions,
  TotalDataParams,
  TotalDataTypes,
} from '@store/interface';

const TotalData = {
  Loading: {
    Set: ({
      key,
      status,
      text,
      subText,
    }: TotalDataParams.SetLoading): TotalDataActions => {
      return {
        type: TotalDataTypes.Loading.SET_LOADING,
        payload: { key, status, text, subText },
      };
    },
  },

  Demo: {
    Save: ({ data }: TotalDataParams.Demo.Save): TotalDataActions => {
      return {
        type: TotalDataTypes.Demo.Save,
        payload: { data },
      };
    },
  },
};

export default TotalData;
