import {
  TotalDataActions,
  TotalDataState,
  TotalDataTypes,
} from '@store/interface';

const initState: TotalDataState = {
  loading: {
    status: false,
  },
  demo: {
    data: '',
  },
};

export const totalReducer = (
  state = initState,
  action: TotalDataActions,
): TotalDataState => {
  switch (action.type) {
    case TotalDataTypes.Loading.SET_LOADING: {
      return {
        ...state,
        loading: action.payload,
      };
    }

    case TotalDataTypes.Demo.Save: {
      const { data } = action.payload;
      return {
        ...state,
        demo: {
          data,
        },
      };
    }

    default:
      return state;
  }
};
