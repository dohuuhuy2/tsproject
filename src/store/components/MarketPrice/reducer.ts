import {
  MarketPriceActions,
  MarketPriceState,
  MarketPriceTypes,
} from '@store/interface';

const initState: MarketPriceState = {
  goldPrice: {
    data: [],
  },
  petrolEum: {
    data: [],
  },
  exchangeRate: {
    data: [],
  },
  news: {
    items: [],
  },
  weather: {
    data: [],
    selectItem: null,
  },
};

export const MarketPriceReducer = (
  state = initState,
  action: MarketPriceActions,
): MarketPriceState => {
  switch (action.type) {
    case MarketPriceTypes.GoldPrice.Response: {
      const { data } = action.payload;
      return {
        ...state,
        goldPrice: {
          data,
        },
      };
    }

    case MarketPriceTypes.PetrolEum.Response: {
      const { data } = action.payload;
      return {
        ...state,
        petrolEum: {
          data,
        },
      };
    }

    case MarketPriceTypes.ExchangeRate.Response: {
      const { data } = action.payload;
      return {
        ...state,
        exchangeRate: {
          data,
        },
      };
    }

    case MarketPriceTypes.News.Response: {
      const { data } = action.payload;
      return {
        ...state,
        news: data,
      };
    }

    case MarketPriceTypes.Weather.Response: {
      const { data } = action.payload;
      return {
        ...state,
        weather: { ...state.weather, data },
      };
    }

    case MarketPriceTypes.Weather.Selected: {
      const { data } = action.payload;
      return {
        ...state,
        weather: { ...state.weather, selectItem: data },
      };
    }

    default:
      return state;
  }
};
