import {
  MarketPriceActions,
  MarketPriceParams,
  MarketPriceTypes,
} from '@store/interface';

const MarketPrice = {
  GoldPrice: {
    Request: ({
      data,
    }: MarketPriceParams.GoldPrice.Request): MarketPriceActions => {
      return {
        type: MarketPriceTypes.GoldPrice.Request,
        payload: { data },
      };
    },

    Response: ({
      data,
    }: MarketPriceParams.GoldPrice.Response): MarketPriceActions => {
      return {
        type: MarketPriceTypes.GoldPrice.Response,
        payload: { data },
      };
    },
  },
  PetrolEum: {
    Request: ({
      data,
    }: MarketPriceParams.PetrolEum.Request): MarketPriceActions => {
      return {
        type: MarketPriceTypes.PetrolEum.Request,
        payload: { data },
      };
    },

    Response: ({
      data,
    }: MarketPriceParams.PetrolEum.Response): MarketPriceActions => {
      return {
        type: MarketPriceTypes.PetrolEum.Response,
        payload: { data },
      };
    },
  },
  ExchangeRate: {
    Request: ({
      data,
    }: MarketPriceParams.ExchangeRate.Request): MarketPriceActions => {
      return {
        type: MarketPriceTypes.ExchangeRate.Request,
        payload: { data },
      };
    },

    Response: ({
      data,
    }: MarketPriceParams.ExchangeRate.Response): MarketPriceActions => {
      return {
        type: MarketPriceTypes.ExchangeRate.Response,
        payload: { data },
      };
    },
  },
  News: {
    Request: ({ data }: MarketPriceParams.News.Request): MarketPriceActions => {
      return {
        type: MarketPriceTypes.News.Request,
        payload: { data },
      };
    },
    Response: ({
      data,
    }: MarketPriceParams.News.Response): MarketPriceActions => {
      return {
        type: MarketPriceTypes.News.Response,
        payload: { data },
      };
    },
  },
  Weather: {
    Request: ({
      data,
    }: MarketPriceParams.Weather.Request): MarketPriceActions => {
      return {
        type: MarketPriceTypes.Weather.Request,
        payload: { data },
      };
    },

    Response: ({
      data,
    }: MarketPriceParams.Weather.Response): MarketPriceActions => {
      return {
        type: MarketPriceTypes.Weather.Response,
        payload: { data },
      };
    },
    selected: ({
      data,
    }: MarketPriceParams.Weather.Selected): MarketPriceActions => {
      return {
        type: MarketPriceTypes.Weather.Selected,
        payload: { data },
      };
    },
  },
};

export default MarketPrice;
