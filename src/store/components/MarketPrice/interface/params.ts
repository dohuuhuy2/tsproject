export namespace MarketPriceParams {
  export namespace GoldPrice {
    export interface Request {
      data?: any;
    }
    export interface Response {
      data: any;
    }
  }
  export namespace PetrolEum {
    export interface Request {
      data?: any;
    }
    export interface Response {
      data: any;
    }
  }
  export namespace ExchangeRate {
    export interface Request {
      data?: any;
    }
    export interface Response {
      data: any;
    }
  }
  export namespace News {
    export interface Request {
      data?: any;
    }
    export interface Response {
      data: any;
    }
  }
  export namespace Weather {
    export interface Request {
      data?: any;
    }
    export interface Response {
      data: any;
    }
    export interface Selected {
      data?: any;
    }
  }
}
