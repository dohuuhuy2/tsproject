export interface MarketPriceState {
  goldPrice: {
    data: any[];
  };
  petrolEum: {
    data: any[];
  };
  exchangeRate: {
    data: any[];
  };
  news: {
    items: any[];
  };
  weather: {
    data: any[];
    selectItem?: any;
  };
}
