export namespace MarketPriceTypes {
  export enum GoldPrice {
    Request = 'GoldPrice -> Request',
    Response = 'GoldPrice -> Response',
  }
  export enum PetrolEum {
    Request = 'PetrolEum -> Request',
    Response = 'PetrolEum -> Response',
  }
  export enum ExchangeRate {
    Request = 'ExchangeRate -> Request',
    Response = 'ExchangeRate -> Response',
  }
  export enum News {
    Request = 'News -> Request',
    Response = 'News -> Response',
  }
  export enum Weather {
    Request = 'Weather -> Request',
    Response = 'Weather -> Response',
    Selected = 'Weather -> Selected',
  }
}
