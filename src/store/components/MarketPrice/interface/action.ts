import { MarketPriceParams, MarketPriceTypes } from '@store/interface';

export type MarketPriceActions =
  | WeatherAction
  | NewsAction
  | GoldPriceAction
  | PetrolEumAction
  | ExchangeRateAction;

export type WeatherAction = Requestweather | Responseweather | Selectedweather;
export interface Selectedweather {
  type: MarketPriceTypes.Weather.Selected;
  payload: MarketPriceParams.Weather.Selected;
}
export interface Requestweather {
  type: MarketPriceTypes.Weather.Request;
  payload: MarketPriceParams.Weather.Request;
}

export interface Responseweather {
  type: MarketPriceTypes.Weather.Response;
  payload: MarketPriceParams.Weather.Response;
}

export type NewsAction = RequestNews | ResponseNews;

export interface RequestNews {
  type: MarketPriceTypes.News.Request;
  payload: MarketPriceParams.News.Request;
}

export interface ResponseNews {
  type: MarketPriceTypes.News.Response;
  payload: MarketPriceParams.News.Response;
}

export type ExchangeRateAction = RequestExchangeRate | ResponseExchangeRate;

export interface RequestExchangeRate {
  type: MarketPriceTypes.ExchangeRate.Request;
  payload: MarketPriceParams.ExchangeRate.Request;
}

export interface ResponseExchangeRate {
  type: MarketPriceTypes.ExchangeRate.Response;
  payload: MarketPriceParams.ExchangeRate.Response;
}

export type PetrolEumAction = RequestPetrolEum | ResponsePetrolEum;

export interface RequestPetrolEum {
  type: MarketPriceTypes.PetrolEum.Request;
  payload: MarketPriceParams.PetrolEum.Request;
}

export interface ResponsePetrolEum {
  type: MarketPriceTypes.PetrolEum.Response;
  payload: MarketPriceParams.PetrolEum.Response;
}

export type GoldPriceAction = RequestGoldPrice | ResponseGoldPrice;

export interface RequestGoldPrice {
  type: MarketPriceTypes.GoldPrice.Request;
  payload: MarketPriceParams.GoldPrice.Request;
}

export interface ResponseGoldPrice {
  type: MarketPriceTypes.GoldPrice.Response;
  payload: MarketPriceParams.GoldPrice.Response;
}
