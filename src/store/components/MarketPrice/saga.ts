import { client } from '@api/instance';
import actionStore from '@store/actionStore';
import { AppState, MarketPriceTypes } from '@store/interface';
import { AxiosResponse } from 'axios';
import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';
import * as DTO from './interface/action';

function* requestGoldPrice({ payload }: DTO.RequestGoldPrice) {
  try {
    const appState: AppState = yield select(state => state);
    const url =
      'http://api.btmc.vn/api/BTMCAPI/getpricebtmc?key=3kd8ub1llcg9t45hnoh8hmn7t5kc2v';
    const response: AxiosResponse = yield call(client.get, url);

    const { data } = response;
    const list = data.DataList.Data;

    if (0) {
      console.info('requestDemo :>> ', { payload, appState, data });
      console.log('list :>> ', list);
    }

    yield put(actionStore.MarketPrice.GoldPrice.Response({ data: list }));
  } catch (error) {
    console.log('error :>> ', error);
  }
}

function* watcher_requestGoldPrice() {
  yield takeLatest(MarketPriceTypes.GoldPrice.Request, requestGoldPrice);
}

function* requestPetrolEum({ payload }: DTO.RequestPetrolEum) {
  try {
    const appState: AppState = yield select(state => state);

    const response: AxiosResponse = yield call(
      client.get,
      'https://service.baomoi.com/energy.json',
      {
        params: {},
        headers: {
          origin: 'https://thanhnien.vn',
          referer: 'https://thanhnien.vn',
        },
      },
    );

    const { data } = response;

    if (0) {
      console.info('requestPetrolEum :>> ', { payload, appState, data });
    }

    yield put(
      actionStore.MarketPrice.PetrolEum.Response({ data: response.data }),
    );
  } catch (error) {
    // clog({ name: 'requestPetrolEum', child: error, type: 'error' });
  }
}

function* watcher_requestPetrolEum() {
  yield takeLatest(MarketPriceTypes.PetrolEum.Request, requestPetrolEum);
}

function* requestExchangeRate({ payload }: DTO.RequestExchangeRate) {
  try {
    const appState: AppState = yield select(state => state);

    const response: AxiosResponse = yield call(
      client.get,
      'https://service.baomoi.com/exchange.json',
      {
        params: {},
        headers: {
          origin: 'https://thanhnien.vn',
          referer: 'https://thanhnien.vn',
        },
      },
    );

    const { data } = response;

    if (0) {
      console.info('requestExchangeRate :>> ', { payload, appState, data });
    }

    yield put(
      actionStore.MarketPrice.ExchangeRate.Response({ data: response.data }),
    );
  } catch (error) {
    // clog({ name: 'requestExchangeRate', child: error, type: 'error' });
  }
}

function* watcher_requestExchangeRate() {
  yield takeLatest(MarketPriceTypes.ExchangeRate.Request, requestExchangeRate);
}

function* requestNews({ payload }: DTO.RequestNews) {
  try {
    const appState: AppState = yield select(state => state);

    const response: AxiosResponse = yield call(
      client.get,
      'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fictnews.vietnamnet.vn%2Frss%2Fcong-nghe.rss',
      { params: {}, headers: {} },
    );

    const { data } = response;

    if (0) {
      console.info('requestNews :>> ', { payload, appState, data });
    }

    yield put(actionStore.MarketPrice.News.Response({ data: response.data }));
  } catch (error) {
    // clog({ name: 'requestNews', child: error, type: 'error' });
  }
}

function* watcher_requestNews() {
  yield takeLatest(MarketPriceTypes.News.Request, requestNews);
}

function* requestWeather({ payload }: DTO.Requestweather) {
  try {
    const appState: AppState = yield select(state => state);

    const response: AxiosResponse = yield call(
      client.get,
      'https://service.baomoi.com/weather-7.json',
      {
        params: {},
        headers: {
          origin: 'https://thanhnien.vn',
          referer: 'https://thanhnien.vn',
        },
      },
    );

    const { data } = response;

    if (0) {
      console.info('requestWeather :>> ', { payload, appState, data });
    }

    yield put(
      actionStore.MarketPrice.Weather.Response({ data: response.data }),
    );
  } catch (error) {
    // clog({ name: 'requestWeather', child: error, type: 'error' });
  }
}

function* watcher_requestWeather() {
  yield takeLatest(MarketPriceTypes.Weather.Request, requestWeather);
}

const martketPriceSagas = function* root() {
  yield all([
    fork(watcher_requestWeather),
    fork(watcher_requestNews),
    fork(watcher_requestGoldPrice),
    fork(watcher_requestPetrolEum),
    fork(watcher_requestExchangeRate),
  ]);
};
export default martketPriceSagas;
