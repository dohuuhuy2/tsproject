import { all, fork } from 'redux-saga/effects';
import martketPriceSagas from './components/MarketPrice/saga';
import totalDataSagas from './components/TotalData/saga';

export default function* rootSaga(): Generator {
  yield all([fork(totalDataSagas), fork(martketPriceSagas)]);
}
