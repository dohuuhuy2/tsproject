import MarketPrice from '@store/components/MarketPrice/action';
import TotalData from '@store/components/TotalData/action';

const actionStore = {
  TotalData,
  MarketPrice,
};

export default actionStore;
