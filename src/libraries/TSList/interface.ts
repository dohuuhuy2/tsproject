import { StyleProp, ViewStyle } from 'react-native';

export interface renderItemProps {
  item: any;
  index: any;
  column?: number;
  selectItem?: any;
  spacing?: number;
}

export interface TSListProps {
  list: any[];
  renderItem: (props: renderItemProps) => JSX.Element;
  column?: number;
  selectItem?: any;
  spacing?: number;
  styleList?: StyleProp<ViewStyle> | undefined;
  styleItem?: StyleProp<ViewStyle> | undefined;
}
