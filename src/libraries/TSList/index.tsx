import { grid } from '@asset/styles/variables';
import { isArray } from 'lodash';
import React from 'react';
import { View } from 'react-native';
import { TSListProps } from './interface';
import { styles } from './styles.module';

const TSList = ({ list, renderItem, ...props }: TSListProps) => {
  const { column = 1, spacing = 1, styleList = {}, styleItem = {} } = props;
  return (
    <View style={[styles.list, styleList]}>
      {isArray(list)
        ? list.map((item, index) => {
            return (
              <View
                key={index}
                style={[styleItem, grid.cell({ column, spacing })]}>
                {renderItem({ item, index, ...props })}
              </View>
            );
          })
        : null}
    </View>
  );
};

export default TSList;
