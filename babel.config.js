module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          '@init': './src/init',
          '@common': './src/common',
          '@store': './src/store',
          '@static': './src/static',
          '@utils': './src/utils',
          '@helper': './src/helper',
          '@api': './src/api',
          '@config': './src/config',
          '@libraries': './src/libraries',
          '@asset': './src/asset',
          '@screen': './src/component/screens',
          '@templates': './src/component/templates',
          '@layouts': './src/component/layouts',
          '@organisms': './src/component/organisms',
          '@molecules': './src/component/molecules',
          '@atoms': './src/component/atoms',
        },
      },
    ],
  ],
};
